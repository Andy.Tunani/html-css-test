# Html-Css Test

Stylesheet :

- pour le relier au Html utiliser
une balise link dans la balise **Head**
  
Sur le CSS, utiliser "alt gr" et 
la touche 4 ou +

Pour entrer la balise spéciale **{}**

_**Désigner la cible (ex : H1) ensuite
entrer la balise spéciale,
entrer la propriété (ex : color) et 
la valeur (ex : red) **_

Les propriété les plus connues : 
- Color = Couleur
- Font = Police
- background-color = coleur arrière plan

Les propiétés pour les **box** : 

- display =
- border = bordure
- width = 
- padding = rajouter marge dans la bordure
- margin = marge (exterieur)


Préciser une modification en écrivant 
sur le **CSS**

(ex : li { color: green;} 
pas de modifications

**.red { color: red;}**
des modifications précises)

et en écrivant sur le **HTMl** 

(ex : <li **_class="red"_**>Test</li>)
pour appliquer la modif°

Chercher la "class" dans le CSS en 
mettant **_un point "."_**
(ex : **_.red_** )

Ensuite entrer la balise spéciale {}

la "class" pour les images : **div.bgi**






